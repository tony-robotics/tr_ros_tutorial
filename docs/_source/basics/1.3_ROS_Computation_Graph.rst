ROS Computation Graph
=====================
**ROS计算图概念**
^^^^^^^^^^^^^^^^^^^

ROS计算图级概念是理解ROS应用的最重要的概念，可以认为ROS计算图是对运行时的ROS应用的形象化表示。

在底层，ROS计算图由ros_comm库实现，包含Master、Node、Topic、Service、Parameter Server等概念的实现。。

下图是一个典型的ROS计算图，我们可以看到用ROS实现的机器人视觉应用项目数据流向。

.. figure:: images/ROS_Computation_Graph示意图.png
    :scale: 60%

**Node:**	 

1. ROS的最小进程单元

2. 从程序角度来说，就是一个可执行文件被执行，加载到了内存之中

3. 从功能角度来说，通常一个node负责机器人的某一个单独的功能

4. ROS Node通过Topic、Service、Action进行通信

5. ROS Node通常由C++或Python实现

**Master:**

1. 节点管理器，在整个网络通信架构里相当于管理中心，管理各个node

2. Node首先在master处进行注册，然后进入整个ROS网络

3. Master引导各个相关节点建立直接的连接（数据的传输不经过Master），实现分布式计算节点间的通信

4. ROS程序启动时，第一步先启动master，随后启动node

**ROS应用的启动**
^^^^^^^^^^^^^^^^^^^^

**1. 启动Master**

打开终端，输入:

::

    roscore

运行roscore命令将启动ROS master，同时启动rosout和parameter server

rosout：负责日志输出的节点，告知用户当前系统的状态，输出系统的error、warning等，并将log记录于日志文件中

parameter server：参数服务器，并不是node，而是存储参数配置的一个服务器

**注意：**
**如果Master在ROS工程启动之后停止或重启，之前启动的节点仍然能正常进行通信。但这些节点将在不会试图与重启之后Master重连，Master不会有之前已经启动的节点的信息，所以不能帮助后续启动的节点与之前启动的节点建立通信连接。**

**2. 启动Node**

可通过以下方式启动节点:

::

    rosrun pkg_name node_name

也可使用以下方式启动同一个节点的多个副本，否则，如果试图启动跟已启动的节点具有相同名字的新节点，会将原有的节点关闭。
::

    rosrun pkg_name node_name __name:=new_name

**注意：**
**如果使用Ctrl-C命令终止节点，通常不会在Master中注销该节点，这样已终止的节点信息依然可以通过rosnode命令查看，可以使用rosnode cleanup命令清除注销节点的残留信息；另外如果使用rosnode kill指令关闭节点，通常不会发生上述现象。**

**ROS计算图示例**
^^^^^^^^^^^^^^^^^^^
1. 首先启动Master

::

    roscore

2. 然后启动turtle模拟器节点

::

    rosrun turtlesim turtlesim_node #启动turtle模拟器

3. 接下来启动turtle控制节点

::

    rosrun turtlesim turtle_teleop_key #通过turtle控制节点，我们可以控制模拟器中的turtle运动

4. 通过rqt node graph查看运行时ROS应用的计算图

:: 

    rqt

在终端中输入rqt，随后在弹出的界面的Plugins中选择Node Graph即可看到下图所示的ROS计算图。


我们可以看到/teleop_turtle Node和/turtlesim Node通过/turtle1/cmd_vel Topic进行通信。

.. figure:: images/turtle_computation_graph.png
    :align: center
    :scale: 60%

**ROS Node 命令行工具**
^^^^^^^^^^^^^^^^^^^^^^^^^

ROS提供了一些命令行工具帮助我们查看Node的信息，最常用的是:

::

    rosnode list #列出当前正在运行的Nodes

::

    rosnode info <node_name> #查看指定Node的信息

更多ROS Node的命令行工具可通过在终端中输入 ``rosnode -h`` 查看

*更多ROS计算图相关的概念可以参考以下内容：*

    http://wiki.ros.org/ROS/Concepts