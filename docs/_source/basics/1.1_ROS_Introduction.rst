ROS_Introduction
================

**ROS简介**
^^^^^^^^^^^^^^

**ROS的应用**

PR2个人助理

.. figure:: images/PR2.png
    :align: center
    :scale: 40%

Turtlebot移动机器人

.. figure:: images/Turtlebot.png
    :align: center
    :scale: 40%

PAL人形机器人

.. figure:: images/PAL.png
    :align: center
    :scale: 40%

NASA太空机器人

.. figure:: images/NASA.png
    :align: center
    :scale: 40%

**ROS的发展历史**

.. figure:: images/ROS_History.png
    :align: center
    :scale: 40%

**Powering the world’s robots—机器人软件的事实标准**

.. figure:: images/ROS_Robot.png
    :align: center
    :scale: 40%

**ROS概念**
^^^^^^^^^^^^^^
ROS：Robot Operating System  机器人操作系统

1. ROS是面向机器人的开源的元操作系统（meta-operating system）

2. 提供类似传统操作系统的诸多功能：硬件抽象、底层设备控制、常用功能实现、进程间消息传递、程序包管理等。

3. 提供相关工具和库，用于获取、编译、编辑代码以及在多个计算机之间运行程序完成分布式计算

ROS为机器人软件开发带来的优势：

1. 分布式计算：点对点，解决进程间通讯问题

2. 软件复用：算法，通信接口， 避免重复造轮子

3. 快速测试：工具，模块化，数据记录与回放

4. 免费开源：ROS软件的开发自始至终采用开放的BSD协议，开源社区

.. figure:: images/ROS_Compose.png
    :align: center
    :scale: 60%

**ROS安装**
^^^^^^^^^^^^^^

.. figure:: images/ROS_Dist.png
    :align: center
    :scale: 50%

我们以ROS Kinetic版本为例看一下ROS的安装及配置。

1. Ubuntu软件配置：勾选universe, restricted, multiverse选项

.. figure:: images/Ubuntu_Software_Configuration.png
    :align: center
    :scale: 50%

2. 添加sources.list：将镜像添加到Ubuntu系统源列表中

::

    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

3. 添加keys：公钥是Ubuntu系统的一种安全机制，ROS安装不可缺少

::

    sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

4. 系统更新：确保Debian软件包和索引是最新的

::

    $ sudo apt-get update && sudo apt-get upgrade

5. 安装ROS：四种安装方式，推荐桌面完整版

::

    $ sudo apt-get install ros-kinetic-desktop-full

**ROS配置**
^^^^^^^^^^^^^^

1. 初始化rosdep:
    rosdep可在需要编译某些源码的时候为其安装一些系统依赖
    rosdep也是某些ROS核心功能组件所必须用到的工具

::

    $ sudo rosdep init && rosdep update

2. ROS环境配置: 添加到bashrc文件中

::

    $ echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

3. 安装rosinstall: ROS中独立分开的常用命令行工具，通过一条命令就可以给某个ROS软件包下载很多源码树。

::

    $ sudo apt-get install python-rosinstall

*更多ROS安装相关的信息可参考以下链接：*
    http://wiki.ros.org/kinetic/Installation/Ubuntu
