3.4 Rviz入门 
==========================
.. image:: rviz_plugin_head.png
   :width: 700px

使用 MoveIt! 最简单的方式是通过 RViz 插件。 Rviz 是 ROS 中调试时所用的主要的可视化和交互工具。 
MoveIt! Rviz 插件使得你可以设置虚拟环境（屏幕），交互式地创建开始和目标状态，测试各种运动规划器和将输出可视化。


第 1 步：启动 Demo 并配置插件
------------------------------------------------

* 启动 demo:

   roslaunch gauss_moveit_config demo.launch

* 如果你第一次执行这条命令，你会在RViz中看到一个空的world。 你需要自行添加一个 Motion Planning 插件。

  * 你会在 RViz 中看到一个空的世界:

  |A|

  * 在 RViz 的 Displays 标签上，点击 *Add*:

  * 在 moveit_ros_visualization 目录上，选择 "MotionPlanning" 作为 the DisplayType。 然后点击 "OK"。

  |B|

  * You should now see the Panda robot in RViz:
  * 你应该在 RViz 中可以看到Gauss 机械臂了：

  |C|

.. |A| image:: rviz_empty.png
               :width: 700px

.. |B| image:: rviz_plugin_motion_planning_add.png
               :width: 300px

.. |C| image:: rviz_start.png
               :width: 700px

* 加载 Motion Planning 完成后，我们需要配置它。在"Displays" 子窗口上的 "Global Options" 标签页，设置 **Fixed Frame** 为 ``groud_link``。

* 现在， 你可以开始为你的 Gauss 机械臂配置插件了。点击 "Displays" 标签上的 "MotionPlanning":

  * 确认 **Robot Description** 设置为 ``robot_description``。

  * 确认 **Planning Scene Topic**  设置为 ``/planning_scene``。

  * 在 **Planning Request**，修改 **Planning Group** 为  ``gauss_arm``。

  * 在 **Planned Path**，把 **Trajectory Topic** 修改为 ``/move_group/display_planned_path``。

.. image:: rviz_plugin_start.png
   :width: 700px

第 2 步：操作可视化的机器人
---------------------------------------
There are four different overlapping visualizations:
有四种不同的重叠的可视化窗口：

#. 在 ``/planning scene`` 的规划环境中的机械臂的配置（默认打开）

#. 机械臂的规划路径（默认打开）

#. 绿色：运动规划的初始状态（默认关闭）

#. 橙色：运动规划的目标状态（默认打开）

这些可视化窗口的显示状态可以使用复选按钮来切换打开和关闭：

#. planning scene 使用 **Scene Robot** 标签页上的 **Show Robot Visual** 复选按钮。

#. planned path 使用  **Planned Path** 标签页上的 **Show Robot Visual** 复选按钮。

#. 初始状态使用 **Planning Request** 标签页上的 **Query Start State** 复选按钮。

#. 目标状态使用 **Planning Request** 标签页上的 **Query Goal State** 复选按钮。

* 点击这些复选按钮来切换不同的可视化窗口的打开和关闭状态。

.. image:: rviz_plugin_visualize_robots.png
   :width: 700px

第 3 步：与 Gauss 机械臂交互
-------------------------------

接下来，我们只需要scene robot、start state 和 goal state。

#. 选中 **Planned Path** 标签页上的 **Show Robot Visual** 复选按钮。

#. 取消选中 **Scene Robot** 标签页上的  **Show Robot Visual** 复选框。

#. 选中  **Planning Request** 标签页上的  **Query Goal State**  复选框。

#. 选中 **Planning Request** 标签页上的  **Query Start State** 复选框。

现在应该可以看到两个交互式的 maker（形状是圆球）。
一个maker对应的一个橙色的机械臂，你可以用来设置运动规划的 "Goal State"。另外一个maker对应的是绿色的机械臂，你可以用来设置运动规划的 "Start State" 。
如果你没有看到交互式的 maker，点击RViz顶部菜单 **Interact**按钮。（注意，有些工具可能被隐藏了，可以点击顶部菜单栏的 **"+"** 来增加 **Interact**）。

.. image:: rviz_plugin_interact.png
   :width: 700px

现在你可以使用这些 maker 来拖拽机械臂和改变它的姿态了。尝试一下吧！

第 4 步：使用 Gauss 机械臂做运动规划
-------------------------------------------

* 现在，你可以在 MoveIt! RViz插件中 使用 Gauss 机械臂做运动规划了。

  * 移动初始姿态到一个期望的位置。

  * 移动目标姿态到另一个期望的位置。

  * 确保两个姿态不会导致机械臂自碰撞。

  * 确保规划的路径是可视化的。检查 **Planned Path** 标签页的 **Show Trail** 为选中状态。

  * 在  **Planning**  标签页的 **MotionPlanning** 窗口上，点击  **Plan** 按钮。你可以看到机械臂开始运动，并显示了运动轨迹。

.. image:: rviz_plugin_planned_path.png
   :width: 700px

* 检查轨迹点：

你可以在RViz上面检查轨迹点：

 * 在 "`Panels`"菜单上，选择 "`MotionPlanning - Slider`"。你会在RViz看到一个新的滑动条面板。

 * 设置你的目标姿态，然后点击`Plan`。

 * 移动滑块，点击 "`Play`" 按钮。

注意： 一旦你设置了末端到新的姿态，确保先按  `Plan` 然后再点击 `Play`。

.. image:: rviz_plugin_slider.png
   :width: 700px


* 保存配置

RViz 允许你保存当前的配置： ``File->Save Config``。