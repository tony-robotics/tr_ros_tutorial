# 3.1 仿真环境 Gazebo 和 RViz

基于 ros 的仿真环境有多种，比如 Gazebo、RViz等。

Gazebo 和 RViz的区别：

rviz是三维可视化工具，强调把已有的数据可视化显示。

gazebo是三维物理仿真平台，强调的是创建一个虚拟的仿真环境。

rviz需要已有数据。rviz提供了很多插件，这些插件可以显示图像、模型、路径等信息，但是前提都是这些数据已经以话题、参数的形式发布，rviz做的事情就是订阅这些数据，并完成可视化的渲染，让开发者更容易理解数据的意义。

gazebo不是显示工具，强调的是仿真，它不需要数据，而是创造数据。我们可以在gazebo中创建一个机器人世界，不仅可以仿真机器人的运动功能，还可以仿真机器人的传感器数据。而这些数据就可以放到rviz中显示，所以使用gazebo的时候，经常也会和rviz配合使用。当我们手上没有机器人硬件或实验环境难以搭建时，仿真往往是非常有用的利器。

Gazebo 的仿真界面：
![gauss_gazebo](gauss_gazebo.png)

RViz 的仿真界面：
![rviz_start](rviz_start.png)

参考：

- http://www.guyuehome.com/2263
