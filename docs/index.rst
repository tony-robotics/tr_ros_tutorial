.. ros_tutorial documentation master file, created by
   sphinx-quickstart on Mon Dec 17 15:53:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ROS培训教程(第2期)
========================================

**欢迎来到Tonyrobotics(汤尼机器人)推出的ROS系列教程！**

准备工作
----------

.. toctree::
   :maxdepth: 1

   _source/setup/0.1.准备工作.rst
   _source/setup/0.2.基础知识预习.rst

理论部分
----------

目标：了解ROS的基础概念

.. toctree::
   :maxdepth: 1

   1.1.ROS简介及安装配置 <_source/basics/1.1_ROS_Introduction.rst>
   1.2.ROS文件层级概念 <_source/basics/1.2_ROS_File_System.rst>
   1.3.ROS计算图级概念 <_source/basics/1.3_ROS_Computation_Graph.rst>
   1.4.ROS Topic <_source/basics/1.4_ROS_Topic.rst>
   1.5.ROS Service <_source/basics/1.5_ROS_Service.rst>
   1.6.ROS Action <_source/basics/1.6_ROS_Action.rst>
   1.7.ROS Launch <_source/basics/1.7_ROS_Launch.rst>

实践部分
----------

目标：从零开始搭建一个基于ROS的移动机器人底盘，最终实现底盘的遥控、建图、避障和导航。

.. toctree::
   :maxdepth: 1

   _source/practice/2.1.ROS基础知识回顾.rst
   _source/practice/2.2.RoboWare安装及使用.rst
   _source/practice/2.3.让底盘动起来！差速控制模型及串口驱动编写.rst
   _source/practice/2.4.将程序放到底盘上以及运动模型的标定.rst
   _source/practice/2.5.标定结果测试.rst
   _source/practice/2.6.激光传感器安装与显示.rst
   _source/practice/2.7.机器人SLAM-建图与导航.rst

机械臂仿真部分
-------------------

.. toctree::
  :maxdepth: 1

  _source/simulation/gazebo_and_rviz.md
  _source/simulation/moveit_intro.md
  _source/simulation/moveit_demo_node.md
  _source/simulation/quickstart_in_rviz_tutorial.rst
  _source/simulation/plan_in_joint_space.md
  _source/simulation/plan_in_cartesian_space.md
